<?php

namespace Uid\Utils\Container;

/** @inheritdoc*/
interface ContainerInterface extends \Psr\Container\ContainerInterface {
    /**
     * Invoke a callable or an object's method.
     *
     * @param string|object $entry The callable or object to invoke.
     * @param string $method The method to invoke (default: '__invoke').
     * @param array $constants Parameters as an associative array to pass to the method (default: []).
     * @return mixed The result of the invocation.
     * @throws ContainerException If the method or callable cannot be invoked.
     * @throws NotFoundException If the entry or method is not found.
     */
    public function invoke(string|object $entry, string $method = '__invoke', array $constants = []);
}
