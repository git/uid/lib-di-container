<?php

namespace Uid\Utils\Container;

use Psr\Container\NotFoundExceptionInterface;

/**
 * No entry was found in the container.
 */
class NotFoundException extends ContainerException implements NotFoundExceptionInterface
{
}
