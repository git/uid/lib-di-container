# lib-di-container

[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)
[![PHP Version](https://img.shields.io/badge/php-%3E%3D8.1-blueviolet)](https://php.net)

A simple implementation of PSR-11: Container.

## Description

`lib-di-container` is a lightweight PHP library that provides a simple implementation of the PSR-11 Container interface. It allows you to manage and retrieve objects and dependencies in your applications.

## Features

- Implements the PSR-11 Container interface.
- Supports object instantiation and retrieval.
- Provides dependency injection and method invocation.

## Installation

You can install `lib-di-container` via [Composer](https://getcomposer.org/):

```bash
composer require uid/lib-di-container
```


## Usage

### Creating a Container

To create a new container, instantiate the `Container` class:

```php
use Uid\Utils\Container\Container;

$container = new Container();
```

### Defining Services

You can define services in the container using the `set` method. Services can be defined as anonymous functions or class names:

```php
// Define a service as an anonymous function
$container->set('myService', function ($container) {
    return new MyService();
});

// Define a service as a class name
$container->set('myService', 'MyService');
```

### Retrieving Services

To retrieve a service from the container, use the `get` method:

```php
$myService = $container->get(MyService::class);
```

### Invoking Methods

The container allows you to invoke methods on objects using the `invoke` method. You can specify the object and method to invoke, along with any required parameters:

```php
$result = $container->invoke($myService, 'someMethod', ['param1' => $value1]);
```

## API Reference

### Container

The `Container` class implements the `Psr\Container\ContainerInterface` interface.

#### Methods

- `__construct(array $definitions = [])`: Creates a new container with optional initial service definitions.
- `value(string $name, mixed $value): self`: Define a value to be stored in the container.
- `alias(string $name, string $alias): self`: Define an alias for a name in the container.
- `set(string $name, string|callable $value, array $constants = [], bool $singleton = false): self`: Defines a service in the container.
- `has(string $id): bool`: Returns true if the container can return an entry for the given identifier.
- `get(string $id): mixed`: Retrieves a service from the container.
- `invoke(string|object $entry, string $method = '__invoke', array $constants = []): mixed`: Invokes a method on an object.

## Requirements

- PHP >= 8.1
- PSR-Container >= 1.0

## Contributing

Contributions are welcome! If you find any issues or have suggestions for improvements, please feel free to submit a pull request.

## License

This project is licensed under the Apache License 2.0. See the [LICENSE](LICENSE) file for more information.
